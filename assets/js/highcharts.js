var chart1;
$(function() {
    chart1 = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
        },
        xAxis: {
            title: {
                text: 'Date'
            }
        },
        yAxis: {
            title: {
                text: 'Kw'
            }
        }
    });
});