$(document).ready(function() {

    $("#form_sign_up").validate({
        rules: {
            email: {
                required: true,
                minlength: 5,
                remote: {
                    url: "/login/login-check",
                    type: "post"
                }
            },
            name: {
                required: true
            },
            surname: {
                required: true
            }
        },
        messages: {
            email: {
                remote: " already exists"
            },
        },
    });

})