$(document).ready(function() {
    $('.button_submit').click(function() {
        var form = $(this).attr('sumbit')
        $("#" + form).submit();
    });

    $('#show_sign_up').click(function() {
        $('#form_sign_in').hide()
        $("#sign_up").fadeIn();
    });


    $("#form_sign_up").submit(function(e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: $(this).serialize(),
            success: function(data)
            {
                $('.alert').html('');
                var msg = JSON.parse(data)
                if (msg.hasOwnProperty("fails")) {
                    $.each(msg['fails'], function(e, val) {
                        $('.alert').css({'color': 'red'}).append('-' + val + '</br>')
                    })
                }
                if (msg.hasOwnProperty("pass")) {
                    $('#sign_up').hide();
                    $('#form_sign_in').fadeIn();
                    $.each(msg['pass'], function(e, val) {
                        $('.alert').css({'color': 'green'}).append(val + '</br>')
                    })
                }
            }
        });

        return false;
    });
})