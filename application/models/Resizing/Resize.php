<?php

Class Resize {

  public $width;
  public $height;

  function __construct($fileName, $img_type) {
    // *** Open up the file
    $this->image = $this->openImage($fileName, $img_type);

    $this->img_type = $img_type;
    // *** Get width and height
    $this->width = imagesx($this->image);

    $this->height = imagesy($this->image);
  }

  ## --------------------------------------------------------

  private function openImage($img_tmp_name, $img_type) {


    if ($img_type == "image/jpeg" || $img_type == "image/pjpeg") {
      $image = imagecreatefromjpeg($img_tmp_name);
    } else if ($img_type == "image/png" || $img_type == "image/x-png") {

      $image = imagecreatefrompng($img_tmp_name);
    }
    return $image;
  }

  ## --------------------------------------------------------

  public function resizeImage($newWidth = 0, $newHeigh = 0) {
    $optimalWidth = $this->width;
    $optimalHeight = $this->height;

    if ($newWidth > 0 && $newHeigh > 0) {
      $optimalWidth = $newWidth;
      $optimalHeight = $newHeigh;
      $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
      imagealphablending($this->imageResized, false);
      imagesavealpha($this->imageResized, true);
      $transparent = imagecolorallocatealpha($this->imageResized, 255, 255, 255, 127);
      imagefilledrectangle($this->imageResized, 0, 0, $optimalWidth, $optimalHeight, $transparent);
      imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
    } else {

      if ($newHeigh > 0) {
        $optimalWidth = $this->getSizeByFixedHeight($newHeigh);
        $optimalHeight = $newHeigh;
      } else if ($newWidth > 0) {
        $optimalHeight = $this->getSizeByFixedWidth($newWidth);
        $optimalWidth = $newWidth;
      }
      // *** Resample - create image canvas of x, y size
      $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
      imagealphablending($this->imageResized, false);
      imagesavealpha($this->imageResized, true);
      $transparent = imagecolorallocatealpha($this->imageResized, 255, 255, 255, 127);
      imagefilledrectangle($this->imageResized, 0, 0, $optimalWidth, $optimalHeight, $transparent);
      imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
    }
  }

  ## --------------------------------------------------------

  public function resizeAtLeastTo($width, $height) {
    $optimalRatio = $width / $height;

    if ($this->width < $width || $this->height < $height) {

      $imgRatio = $this->width / $this->height;
      if ($optimalRatio > 1) {          //obrazek docelowo szerszy
        if ($imgRatio >= $optimalRatio) {
          $optimalWidth = $this->getSizeByFixedHeight($height);
          $optimalHeight = $height;
        } else {
          $optimalHeight = $this->getSizeByFixedWidth($width);
          $optimalWidth = $width;
        }
      } else {                          //obrazek docelowo wyzszy
        if ($imgRatio <= $optimalRatio) {
          if ($imgRatio > $optimalRatio) {
            $optimalHeight = $this->getSizeByFixedWidth($width);
            $optimalWidth = $width;
          } else {
            $optimalWidth = $this->getSizeByFixedHeight($height);
            $optimalHeight = $height;
          }
        }
      }

      // *** Resample - create image canvas of x, y size
      $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
      imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
    } else {

      $optimalHeight = $this->height;
      $optimalWidth = $this->width;

      // *** Resample - create image canvas of x, y size
      $this->imageResized = imagecreatetruecolor($optimalWidth, $optimalHeight);
      imagecopyresampled($this->imageResized, $this->image, 0, 0, 0, 0, $optimalWidth, $optimalHeight, $this->width, $this->height);
    }
  }

  ## --------------------------------------------------------

  public function getSizeByFixedHeight($newHeight) {
    $ratio = $this->width / $this->height;
    $newWidth = $newHeight * $ratio;
    return $newWidth;
  }

  public function getSizeByFixedWidth($newWidth) {
    $ratio = $this->height / $this->width;
    $newHeight = $newWidth * $ratio;
    return $newHeight;
  }

  //rotate
  public function rotateP($d = -90) {
    $this->imageResized = imagerotate($this->image, $d, 0);
  }

  public function saveImage($savePath) {

    // *** Get extension
    if ($this->img_type == "image/jpeg" || $this->img_type == "image/pjpeg") {

      imagejpeg($this->imageResized, $savePath, 100);
    } else if ($this->img_type == "image/png" || $this->img_type == "image/x-png") {

      imagepng($this->imageResized, $savePath, 0);
    }


    imagedestroy($this->imageResized);
  }

  ## --------------------------------------------------------
}

?>
