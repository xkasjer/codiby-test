<?php

require_once 'Db/Db_Db.php';

class Energy {

  public static $table = 'energy';

  public function __construct($idUser) {
    parent::__construct(NULL, $idUser);
  }

  public static function getAll($date_start, $date_end) {
    $db = Db_Db::conn();
    $sql = $db->select()->from(self::$table, array('day', 'kw' => 'sum(kw)'));
    $sql->where('day>=?', $date_start)->where('day<=?', $date_end)
         ->group('day')
    ;
    //echo $sql;
    return $db->fetchAll($sql);
  }

  public static function getLastDay() {
    $db = Db_Db::conn();
    $sql = $db->select()->from(self::$table, array('day'))
              ->order('day DESC')->limit(1);
    return $db->fetchOne($sql);
  }

}
