<?php

require_once "Resizing/Resize.php";

class Images {

  public static function uploadImg($imgArray, $table, $path, $destMiniSize = array('x' => 0, 'y' => 50)) {

    $image = $imgArray['tmp_name'];
    $image_type = $imgArray['type'];
    $nazwaImg = $imgArray['name'];
    $nazwaImg = Db_Db::isNameFree($nazwaImg, $table, 'name_images');

    //zapisywanie nieprzeskalowanego img
    move_uploaded_file($image, $path . '' . $nazwaImg);

    //zapisywanie przeskalowanego img
    $imgMini = new Resize($path . '' . $nazwaImg, $image_type);
    $imgMini->resizeImage($destMiniSize['x'], $destMiniSize['y']);
    $imgMini->saveImage($path . 'thumb/' . $nazwaImg);

    return array('path' => $nazwaImg);
  }

}

?>
