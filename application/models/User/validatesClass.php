<?php

class validatesClass {

  public static function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
      $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
      $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
      $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
      $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
      $ipaddress = getenv('REMOTE_ADDR');
    else
      $ipaddress = 'UNKNOWN';
    return $ipaddress;
  }

  public static function CheckEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
  }

  public static function CheckPESEL($str) {
    if (!preg_match('/^[0-9]{11}$/', $str)) {
      return false;
    }

    $arrSteps = array(1, 3, 7, 9, 1, 3, 7, 9, 1, 3);
    $intSum = 0;
    for ($i = 0; $i < 10; $i++) {
      $intSum += $arrSteps[$i] * $str[$i];
    }
    $int = 10 - $intSum % 10;
    $intControlNr = ($int == 10) ? 0 : $int;
    if ($intControlNr == $str[10]) {
      return true;
    }
    return false;
  }

  public static function notEmpty($value) {
    if (strlen(trim($value)))
      return true;
    return false;
  }

  public static function checkDate($Date) {
    if (preg_match("/^[0-9]{4}-[01][0-9]-[0-3][0-9]$/D", $Date)) {
      list( $year, $month, $day ) = explode('-', $Date);
      return checkdate($month, $day, $year);
    } else {
      return false;
    }
  }

  public static function checkPassword($pass) {
    if (preg_match("/(?=.{2,})(?=.*[A-Z])(?=.*[0-9])/", $pass) && strlen($pass) >= 8)
      return true;

    return false;
  }

}
?>

