<?php

class User {

  protected $db = "";
  protected $id;
  protected $login;
  private $main_table = "users";

  public function __construct($login, $idUser = null) {
    $this->db = Db_Db::conn();
    $this->login = $login;
    $this->id = $idUser;


    if ($login == null) {

      $login = "SELECT `email` FROM $this->main_table WHERE `id` = '" . $this->id . "'";

      $this->login = $this->db->fetchOne($login);

      if ($this->login) {
        return new User($this->login, $this->db);
      } else {
        throw new UserException("Invalid login adress ", "Podany adres email nie został znaleziony!");
      }
    } else {
      $login = "SELECT `email` FROM $this->main_table WHERE `email` = '" . $this->login . "'";
      $this->login = $this->db->fetchOne($login);
      if (!$this->login) {
        throw new UserException("Invalid login adress ", "Podany adres login nie został znaleziony!");
      }
    }
  }

  public static function getAll($limit = 0, $page = 0) {
    try {
      $db = Db_Db::conn();
      $sql = $db->select()->from('users');
      if ($limit != 0)
        $sql->limit($limit, $limit * $page);
      return $db->fetchAll($sql);
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

  public static function checkLogin($value) {
    try {
      $db = Db_Db::conn();
      $sql = $db->select()->from('users')->where('email=?', $value);
      $result = $db->fetchAll($sql);
      if (count($result))
        return true;
      return false;
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

  public function getLogin() {
    return $this->login;
  }

  public function getId() {
    if ($this->id) {
      return $this->id;
    } else {
      try {
        $id = "SELECT `id` FROM $this->main_table WHERE `email` = '" . $this->login . "'";
        $this->id = $this->db->fetchOne($id);
        return $this->id;
      } catch (Zend_Db_Exception $DbE) {
        echo $DbE->getMessage();
      }
    }
  }

  public static function checkEmail($value) {
    try {
      $db = Db_Db::conn();
      $sql = $db->select()->from('users')->where('email=?', $value);
      $result = $db->fetchAll($sql);
      if (count($result))
        return true;
      return false;
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

  public static function checkData($array_param) {
// allow email, name, surname, date_birth
    require_once 'validatesClass.php';
    $message = array();
    if (self::checkEmail($array_param['email']))
      array_push($message, 'Email is already exists.');
    if (!validatesClass::notEmpty($array_param['email']) or !validatesClass::CheckEmail($array_param['email']))
      array_push($message, 'Email is not pass.');
    if (!validatesClass::notEmpty($array_param['name']) or !validatesClass::notEmpty($array_param['surname']))
      array_push($message, 'Name and surname is not allow empty');
    if (!validatesClass::checkDate($array_param['date_birth']))
      array_push($message, 'Date birth is not pass');
    return $message;
  }

  public static function addAcount($arr_value) {
    try {
      $data = array();
      $data['email'] = $arr_value['email'];
      $data['name'] = $arr_value['name'];
      $data['surname'] = $arr_value['surname'];
      $data['date_birth'] = $arr_value['date_birth'];
      $pass = self::passGenerate(6);
      $data['password'] = md5($pass);
      $db = Db_Db::conn();
      $count = $db->insert('users', $data);
      if (count($count))
        return $pass;
      return false;
    } catch (Zend_Db_Exception $DbE) {
      echo $DbE->getMessage();
    }
  }

  public static function passGenerate($dlugosc) {
    $zestaw_znakow = "1234567890qwertyuioplkjhgfdsazxcvbnmQWERTYUIOPLKJHGFDSAZXCVBNM";
    $haslo = '';
    for ($i = 0; $i <= $dlugosc - 1; $i++) {
      $losowy = rand(0, strlen($zestaw_znakow) - 1);
      $haslo .= $zestaw_znakow{$losowy};
    }
    return $haslo;
  }

}

class UserException extends Exception {

  private $wrongData;

  public function __construct($message = null, $wrongData, $code = 0) {
    $this->wrongData = $wrongData;
    $t_message = "Exception raised in User ";
    $t_message .= "with message : " . $message;
    $t_message .= " on line : " . $this->getLine();

    parent::__construct($t_message, $code);
  }

  public function getWrongData() {
    return $this->wrongData;
  }

}

?>
