<?php

class Email {

  private static $from = "noreply@mailkong.com";
  private static $titleFrom = "Energy System";
  private static $miesiace = array(1 => 'Styczeń', 2 => 'Luty', 3 => 'Marzec', 4 => 'Kwiecień', 5 => 'Maj', 6 => 'Czerwiec', 7 => 'Lipiec', 8 => 'Wrzesień', 9 => 'Październik', 11 => 'Listopad', 12 => 'Grudzień');

  public static function sendEmail($to, $subject, $message) {
    $message.="<br>
Tast Test";
    $mail = new Zend_Mail('UTF-8');
    $mail->setSubject($subject)
         ->addTo($to)
         ->setBodyHtml($message)
         ->setFrom(Email::$from, Email::$titleFrom)
         ->send();
  }

  public static function createAccount($to, $name, $pass) {
    $subject = "Create new account";
    $message = "Hello " . $name . ",<br>
Pass for your account: " . $pass . "<br><br>";
    Email::sendEmail($to, $subject, $message);
  }

}

?>
