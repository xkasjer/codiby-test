<?php

require_once 'User/User.php';

Login::$auth = Zend_Auth::getInstance();

class Login {

  static $auth = "";
  private $request = "";
  private $db = "";
  private $messages = '';
  private $main_table = "users";

  public function __construct($request) {
    $this->db = Db_Db::conn();
    $this->request = $request;
  }

  public function getIdentity() {
    return Login::$auth->getIdentity();
  }

  public function getUser() {
    return new User($this->getIdentity());
  }

  public function getMessages() {
    return $this->messages;
  }

  public function tryLogin() {

    $auth = Login::$auth;

    if ($this->getIdentity()) {
      return true;
    }

    if ($this->request->isPost()) {
      try {
        $adapter = new Zend_Auth_Adapter_DbTable($this->db);

        $adapter
             ->setTableName($this->main_table)
             ->setIdentityColumn('email')
             ->setCredentialColumn('password');
        $adapter
             ->setIdentity($this->request->getPost('email'))
             ->setCredential(md5($this->request->getPost('password')));
        $result = $auth->authenticate($adapter);
        if ($result->getCode() == Zend_Auth_Result::SUCCESS) {
          return true;
        } else {
          $this->messages = "There was an error with your email/password combination. Please try again.";
          return false;
        }
      } catch (Zend_Auth_Adapter_Exception $e) {
        $this->messages = "There was an error with your email/password combination. Please try again.";
        return false;
      }
    } else {
      return false;
    }
  }

  public function logout() {
    Login::$auth->clearIdentity();
  }

}

?>
