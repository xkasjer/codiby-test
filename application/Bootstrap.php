<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {

  protected function _initFlashMessenger() {

    $this->bootstrap('view');

    $view = $this->getResource('view');
  }

  protected function _initModules() {
    $front = Zend_Controller_Front::getInstance();
    $front->registerPlugin(new Application_Plugin_ModuleLoader());

    $router = $front->getRouter();
  }

  protected function _initDoctype() {
    $this->bootstrap('view');
    $view = $this->getResource('view');
    $view->doctype('HTML5');
  }

  protected function _initHeadLink() {
    $this->bootstrap('view');
    $view = $this->getResource('view');
  }

  protected function _initHeadStyle() {
    $this->bootstrap('view');
    $view = $this->getResource('view');

    $view->headLink()
         ->appendStylesheet('/assets/css/style.css')
         ->appendStylesheet('/assets/themes_tablesorter/blue/style.css');
  }

  protected function _initTitle() {
    $view = $this->getResource('view');
    $this->view->headTitle()->setSeparator(' - ');
    $this->view->headTitle()->prepend('Erengy');
  }

  protected function _initHeadScript() {
    $this->bootstrap('view');
    $view = $this->getResource('view');
    $view->headScript()->appendFile('/assets/js/jquery.min.js')
         ->appendFile('/assets/js/jquery.tablesorter.min.js')
         ->appendFile('/assets/js/script.js')
    ;
  }

  protected function _initHeadMeta() {
    $this->bootstrap('view');
    $view = $this->getResource('view');

    $view->headMeta()->appendHttpEquiv('Content-Type', 'text/html; charset=UTF-8')
         ->appendHttpEquiv('X-UA-Compatible', 'IE=9');
    $view->headMeta()->setName('robots', 'index,follow,all');


    //
  }

  protected function _initInclude() {
    require_once "Db/Db_Db.php";
    require_once "User/User.php";
    require_once "Login/Login.php";
    require_once "Energy/Energy.php";
  }

}
