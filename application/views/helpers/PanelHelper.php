<?php

class Zend_View_Helper_PanelHelper extends Zend_View_Helper_Abstract {

  public function panelHelper($db, $login) {
    $iduser = null;
    if ($login) {
      $this->view->islogged = true;
      $iduser = $login->getId();
    }
    return $this->view->render('helpers/panel/panel.phtml');
  }

}

?>
