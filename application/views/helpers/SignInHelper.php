<?php

class Zend_View_Helper_SignInHelper extends Zend_View_Helper_Abstract {

  public function signInHelper($login) {
    if ($login) {
      $this->view->error_login = $login->getMessages();
    }
    return $this->view->render('helpers/sign-in/sign-in.phtml');
  }

}

?>
