<?php

class LoginController extends Zend_Controller_Action {

  public $panelSession;

  public function init() {
    require_once "Db/Db_Db.php";
    try {
      $this->db = Db_Db::conn();
    } catch (Zend_Db_Exception $e) {
      echo $e->getMessage();
    }

    //autoryzacja
    require_once "Login/Login.php";
  }

  public function logoutAction() {
    $login = new Login($this->getRequest());
    $login->logout();
    $this->_redirect("/");
    $this->_helper->viewRenderer->setNoRender();
  }

  public function loginCheckAction() {
    $check = User::checkLogin($this->_request->getParam('email'));
    if ($check) {
      echo "false";
    } else {
      echo "true";
    }
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
  }

  public function signUpAction() {
    $check = User::checkData($this->_request->getParams());
    if (count($check) == 0) {
      if ($pass = User::addAcount($this->_request->getParams())) {
        require_once 'Email/Email.php';
        Email::createAccount($this->_request->getParam('email'), $this->_request->getParam('name'), $pass);
        echo json_encode(array('pass' => array('Create account. We send email with password to your email.')));
      } else {
        echo json_encode(array('fails' => array('Fail create account.')));
      }
    } else {
      echo json_encode(array('fails' => $check));
    }
    $this->_helper->viewRenderer->setNoRender();
    $this->_helper->layout()->disableLayout();
  }

}
