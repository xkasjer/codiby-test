<?php

class IndexController extends Zend_Controller_Action {

  public function init() {
    $this->view->login = $this->login = new Login($this->getRequest());
    if ($this->login->tryLogin()) {
      $this->view->user = $this->user = $this->login->getUser();
    }
  }

  public function indexAction() {
    $this->view->headScript()->appendFile('http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.1/jquery.validate.min.js')
         ->appendFile('/assets/js/validate.js');
  }

  public function usersAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    $this->view->all_user = User::getAll();
  }

  public function energyAction() {
    if (!$this->login->tryLogin()) {
      $this->_redirect("/");
    }
    $this->view->date_start = $this->_getParam('date_start');
    $this->view->date_end = $this->_getParam('date_end');
    if ($this->view->date_end == NULL) {
      $this->view->date_end = Energy::getLastDay();
    }
    if ($this->view->date_start == NULL) {
      $date_start = new DateTime("first day of " . date("F Y", strtotime($this->view->date_end)));
      $this->view->date_start = $date_start->format('Y-m-d');
    }
    $this->energy = Energy::getAll($this->view->date_start, $this->view->date_end);
    foreach ($this->energy as $energy) {
      $data[] = "[" . (strtotime($energy['day']) * 1000) . ", " . $energy['kw'] . "]";
    }
    $this->view->energy = $data;
    if ($this->_request->isXmlHttpRequest()) {
      echo '[' . join($data, ',') . ']';
      $this->_helper->layout()->disableLayout();
      $this->_helper->viewRenderer->setNoRender();
    } else {
      $this->view->headScript()->appendFile('http://code.highcharts.com/highcharts.js')
           ->appendFile('/assets/js/jquery.ui.js');
      $this->view->headLink()->appendStylesheet('/assets/css/jquery-ui.css');
    }
  }

}
